package talent.alpha.wordslider.utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
public class WordUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();
    private static StringBuilder contentBuilder = new StringBuilder();

    private static String readFile(String filePath) {
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            log.error("Error during parsing file {}", filePath, e);
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public static List<String> getStringListFromJsonResource(Resource resourceFile) {
        List<String> stringList = new ArrayList();
        try {
            stringList = objectMapper.readValue(
                readFile(resourceFile.getFile().getPath()),
                List.class);
        } catch (IOException e) {
            log.error("Error during parsing resource file", e);
            e.printStackTrace();
        }
        return stringList;
    }

}