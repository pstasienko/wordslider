package talent.alpha.wordslider.service;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

/**
 * Basic unit tests.
 *
 */
public class InputToSlidesProcessorUT {


    List<String> expectedSlidesList = Collections.EMPTY_LIST;

    @Before
    public void init() {
        expectedSlidesList = prepareExpectedSlidesList();
    }

    @Test
    public void stringEntriesReturnedByRepositoryMock__notEmptyWordMapReturnedByService() {
        String userInputText = "Mary went Mary's gone";
        List<String> actualSlidesList = new ArrayList<>();
        actualSlidesList = InputToSlidesProcessor.getListOfSlides(actualSlidesList, userInputText);
        assertThat(actualSlidesList, hasSize(10));
        assertThat(actualSlidesList, containsInAnyOrder(expectedSlidesList.toArray()));
    }

    @After
    public void tearDown() {
        expectedSlidesList.clear();
    }

    private List<String> prepareExpectedSlidesList() {
        List<String> returnList = new ArrayList<>();
        returnList.add("Mary went Mary's gone");
        returnList.add("Mary went Mary's");
        returnList.add("went Mary's gone");
        returnList.add("went Mary's");
        returnList.add("Mary's gone");
        returnList.add("Mary went");
        returnList.add("Mary's");
        returnList.add("Mary");
        returnList.add("went");
        returnList.add("gone");
        return returnList;
    }

}