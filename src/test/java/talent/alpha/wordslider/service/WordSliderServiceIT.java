package talent.alpha.wordslider.service;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import talent.alpha.wordslider.model.Word;
import talent.alpha.wordslider.repository.WordRepository;
import lombok.extern.slf4j.Slf4j;
import talent.alpha.wordslider.utils.WordUtil;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.hamcrest.collection.IsMapContaining;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.not;

/**
 * Basic integration tests.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class WordSliderServiceIT {

    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private WordSliderService wordSliderService;

    @Value("classpath:data/wordslider_testdata.json")
    private Resource wordSliderResourceJsonFile;

    private List<String> stringList = Collections.EMPTY_LIST;
    private List<Word> wordList = Collections.EMPTY_LIST;

    @Before
    public void init() {
        log.info("Storing example data in redis store");
        stringList = WordUtil.getStringListFromJsonResource(wordSliderResourceJsonFile);
        wordList = stringList.stream().map(stringEntry -> new Word(stringEntry)).collect(Collectors.toList());
        wordRepository.saveAll(wordList);
    }

    @Test
    public void stringEntriesStoredInRedisAndMatching__notEmptyWordMapReturnedByService() {
        String userInputText = "Mary went Mary's gone";
        Map<String, Integer> wordMap = wordSliderService.getSlidesMap(userInputText);
        assertThat(wordMap.size(), is(2));
        assertThat(wordMap, IsMapContaining.hasKey("went Mary's"));
        assertThat(wordMap, IsMapContaining.hasKey("Mary"));
        assertThat(wordMap, not(IsMapContaining.hasKey("Mary gone")));
    }

    @Test
    public void stringEntriesStoredInRedisAndMatching__emptyWordMapReturnedByService() {
        String userInputText = "Input test without matching entry in redis";
        Map<String, Integer> wordMap = wordSliderService.getSlidesMap(userInputText);
        assertThat(wordMap.size(), is(0));
    }

    @After
    public void tearDown() {
        wordRepository.deleteAll(wordList);
    }

}