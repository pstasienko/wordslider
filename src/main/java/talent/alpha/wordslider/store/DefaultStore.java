package talent.alpha.wordslider.store;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import talent.alpha.wordslider.exception.StoreException;
import talent.alpha.wordslider.model.Word;
import talent.alpha.wordslider.repository.WordRepository;
import java.util.List;
import java.util.Optional;

/**
 * Store implementation.
 *
 */
@Slf4j
@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true)
public class DefaultStore implements Store {

    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Boolean exists(String entryToCheck) {
        return redisTemplate.hasKey(Word.WORD_HASH + ":" + entryToCheck);
    }

    @Override
    public Integer getValue(String id) {
        Optional<Word> retrievedValue = wordRepository.findById(id);
        if(retrievedValue.isPresent()) {
            return retrievedValue.get().getWordValue();
        }
        else {
            log.error("Input value for id={} not found in a store", id);
            throw new StoreException("Input value not found in a store");
        }
    }

    public Boolean anyEntryExistsInStore(List<String> stringList) {
        if(stringList.isEmpty()) {
            return false;
        }
        for(String entry:stringList) {
            if(exists(entry)) {
                return true;
            }
        }
        return false;
    }

}