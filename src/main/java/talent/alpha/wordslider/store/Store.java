package talent.alpha.wordslider.store;


import java.util.List;

/**
 * Store.
 *
 */
public interface Store {
    Boolean exists(String entryToCheck);
    Integer getValue(String id);
    Boolean anyEntryExistsInStore(List<String> stringList);
}