package talent.alpha.wordslider.service;


import java.util.Map;

/**
 * Word slides service.
 *
 */
public interface WordSliderService {
    Map<String, Integer> getSlidesMap(String userInputText);
}
