package talent.alpha.wordslider.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import talent.alpha.wordslider.store.Store;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Word slides service implementation.
 * Since slide is consumed from the input string, each time the specific slide is found new iteration is needed.
 *
 */
@Slf4j
@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true)
public class DefaultWordSliderService implements WordSliderService {

    @Autowired
    private Store store;

    @Override
    public Map<String, Integer> getSlidesMap(String userInputText) {
        Map<String, Integer> returnMap = new HashMap<>();
        List<String> stringList = new ArrayList<>();
        stringList = InputToSlidesProcessor.getListOfSlides(stringList, userInputText);

        while(store.anyEntryExistsInStore(stringList)) {
            for(String entry:stringList) {
                if(store.exists(entry)) {
                    returnMap.put(entry, store.getValue(entry));
                    userInputText = StringUtils.delete(userInputText, entry);
                    stringList.clear();
                    stringList = InputToSlidesProcessor.getListOfSlides(stringList, userInputText);
                    break;
                }
            }
        }
        return returnMap;
    }

}