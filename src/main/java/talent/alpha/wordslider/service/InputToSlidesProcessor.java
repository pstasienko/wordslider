package talent.alpha.wordslider.service;


import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * User input string processor.
 * Recursive algorithm that from the given user string input creates list of all possible slides.
 * If input is a string without whitespaces it is returned and method run is finished.
 * Otherwise algorithm cat the last word from the input and run once again for a newly created input.
 * After finishing this part first word from a string is cut and algorithm is run again for such string.
 * Finally redundant entries are removed.
 *
 */
public class InputToSlidesProcessor {

    public static List<String> getListOfSlides(List<String> list, String userInputString) {
        List<String> listOfSlides = getListOfSlidesWithDuplicate(list, userInputString);
        listOfSlides = listOfSlides.stream()
                .distinct()
                .collect(Collectors.toList());
        Collections.sort(listOfSlides, Comparator.comparing(String::length).reversed());
        return listOfSlides;
    }


    private static List<String> getListOfSlidesWithDuplicate(List<String> list, String userInputString) {
        userInputString = StringUtils.trimWhitespace(userInputString);
        if(StringUtils.isEmpty(userInputString)) {
            return Collections.EMPTY_LIST;
        }
        else if(!StringUtils.containsWhitespace(userInputString)) {
            list.add(userInputString);
            return list;
        }
        else {
            List<String> newList = new ArrayList<>();
            newList.add(userInputString);
            newList.addAll(getListOfSlides(list, userInputString.replaceAll("[^\\s]+$", "")));
            newList.addAll(getListOfSlides(list, userInputString.replaceAll("^[^\\s]+", "")));
            return newList;
        }
    }

}