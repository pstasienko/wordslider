package talent.alpha.wordslider.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import talent.alpha.wordslider.model.Word;
import java.util.Optional;

/**
 * Simple repository to be picked up by the Redis repository support.
 *
 */
@Repository
public interface WordRepository extends CrudRepository<Word, String> {
    Optional<Word> findById(String id);
}