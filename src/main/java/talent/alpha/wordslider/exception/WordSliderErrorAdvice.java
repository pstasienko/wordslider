package talent.alpha.wordslider.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import talent.alpha.wordslider.model.Message;
import talent.alpha.wordslider.model.MessageLevel;
import talent.alpha.wordslider.model.VoidRestResponse;

/**
 * Error handling configuration.
 *
 */
@ControllerAdvice
@Slf4j
public class WordSliderErrorAdvice {

    @ExceptionHandler({EmptyUserTextException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public VoidRestResponse handleEmptyUserTextException(EmptyUserTextException e) {
        VoidRestResponse voidResponse = new VoidRestResponse();
        Message message = Message.builder().level(MessageLevel.ERROR).message(e.getMessage()).build();
        voidResponse.getMessages().add(message);
        return voidResponse;
    }

    @ExceptionHandler({NoSlidesFoundException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public VoidRestResponse handleNoSlidesFoundException(NoSlidesFoundException e) {
        VoidRestResponse emptyResponse = new VoidRestResponse();
        Message message = Message.builder().level(MessageLevel.INFO).message(e.getMessage()).build();
        emptyResponse.getMessages().add(message);
        return emptyResponse;
    }

    @ExceptionHandler({StoreException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public VoidRestResponse handleStoreException(StoreException e) {
        VoidRestResponse errorResponse = new VoidRestResponse();
        Message message = Message.builder().level(MessageLevel.ERROR).message(e.getMessage()).build();
        errorResponse.getMessages().add(message);
        return errorResponse;
    }

}