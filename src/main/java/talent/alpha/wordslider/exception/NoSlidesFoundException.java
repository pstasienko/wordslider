package talent.alpha.wordslider.exception;

public class NoSlidesFoundException extends RuntimeException {
    public NoSlidesFoundException(String message) {
        super(message);
    }
}