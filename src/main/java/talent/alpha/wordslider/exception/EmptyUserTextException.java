package talent.alpha.wordslider.exception;

public class EmptyUserTextException extends RuntimeException {
    public EmptyUserTextException(String message) {
        super(message);
    }
}