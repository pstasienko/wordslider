package talent.alpha.wordslider.rest;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import talent.alpha.wordslider.exception.EmptyUserTextException;
import talent.alpha.wordslider.exception.NoSlidesFoundException;
import talent.alpha.wordslider.facade.WorkSliderFacade;
import talent.alpha.wordslider.model.WordSliderResponse;
import java.util.Map;

/**
 * Word slides controller exposing REST API operations.
 *
 */
@Slf4j
@RestController
@Api(value = "word-slider-service", description = "Operations used for managing words transformation")
public class WordSliderResource {

    private static final String WORD_SLIDER_END_POINT = "/wordslider";

    @Autowired
    private WorkSliderFacade workSliderFacade;

    @ApiOperation(value = "Find slides of a input string based on the entries kept in a store.", response = Map.class)
    @GetMapping(WORD_SLIDER_END_POINT)
    public ResponseEntity<WordSliderResponse> getSlidesMap(String userInputText) {
        if(StringUtils.isEmpty(userInputText)) {
            log.info("Empty user input text");
            throw new EmptyUserTextException("Bad request - empty user input text");
        }
        Map<String, Integer> slidesMap = workSliderFacade.getSlidesMap(userInputText);
        if (!slidesMap.isEmpty()) {
            log.info("Slides were found in following user input text: {}", userInputText);
            WordSliderResponse wordSliderResponse = new WordSliderResponse(slidesMap);
            return new ResponseEntity<>(wordSliderResponse, new HttpHeaders(), HttpStatus.OK);
        } else {
            log.info("No slides found in a user input string");
            throw new NoSlidesFoundException("No slides found in a user input string");
        }
    }
}