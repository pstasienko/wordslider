package talent.alpha.wordslider.facade;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import talent.alpha.wordslider.service.WordSliderService;
import java.util.Map;

/**
 * Word Slider facade configuration.
 *
 */
@Service
@Slf4j
@Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true)
public class DefaultWordSliderFacade implements WorkSliderFacade {

    @Autowired
    private WordSliderService wordSliderService;

    @Override
    public Map<String, Integer> getSlidesMap(String userInputText) {
        return wordSliderService.getSlidesMap(userInputText);
    }
}