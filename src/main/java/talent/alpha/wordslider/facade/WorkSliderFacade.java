package talent.alpha.wordslider.facade;


import java.util.Map;

/**
 * Word Slider facade.
 *
 */
public interface WorkSliderFacade {
    Map<String, Integer> getSlidesMap(String userInputText);
}