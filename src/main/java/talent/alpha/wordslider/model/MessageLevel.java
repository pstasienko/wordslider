package talent.alpha.wordslider.model;


/**
 * Messages levels.
 *
 */
public enum MessageLevel {
    ERROR, WARNING, INFO, DEBUG
}