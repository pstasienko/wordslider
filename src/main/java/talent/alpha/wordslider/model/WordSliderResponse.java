package talent.alpha.wordslider.model;


import java.util.List;
import java.util.Map;

/**
 * Word slider REST Response object to handle payload and massages returned to the client.
 *
 */
public class WordSliderResponse extends RestResponse<Map> {
    public WordSliderResponse(List<Message> messages) {
        super(messages);
    }
    public WordSliderResponse(Map slidesMapPayload) {
        super(slidesMapPayload);
    }
}