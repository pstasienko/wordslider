package talent.alpha.wordslider.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Response object to handle payload and massages.
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
public abstract class RestResponse<T> {
    private T payload;
    private List<Message> messages;

    public RestResponse() {
        this.messages = new ArrayList<>();
    }
    public RestResponse(List<Message> messages) {
        this.messages = messages;
    }
    public RestResponse(T payload) {
        this.payload = payload;
    }
}