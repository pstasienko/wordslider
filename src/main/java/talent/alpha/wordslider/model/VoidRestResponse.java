package talent.alpha.wordslider.model;


/**
 * Empty REST Response object.
 *
 */
public class VoidRestResponse extends RestResponse<Void> {
}