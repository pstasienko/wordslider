package talent.alpha.wordslider.model;


import lombok.*;

/**
 * Messages returned to client with suitable information regarding method call output.
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class Message {
    private String message;
    private MessageLevel level;
}