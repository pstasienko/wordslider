package talent.alpha.wordslider.model;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import javax.validation.constraints.NotNull;
import java.util.Random;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@RedisHash(Word.WORD_HASH)

/**
 * Word object stored inside a Redis
 */
public class Word {

    public static final String WORD_HASH = "word";
    private int MIN_WORD_VALUE = 1;
    private int MAX_WORD_VALUE = 10;

    @Id
    @NotNull
    private String word;

    private Integer wordValue = new Random().ints(MIN_WORD_VALUE, MAX_WORD_VALUE+1).findFirst().getAsInt();

    public Word(@NotNull String word) {
        this.word = word;
    }

}