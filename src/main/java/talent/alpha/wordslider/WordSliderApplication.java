package talent.alpha.wordslider;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot application.
 *
 */
@SpringBootApplication
public class WordSliderApplication {
    public static void main(String[] args) {
        SpringApplication.run(WordSliderApplication.class, args);
    }
}