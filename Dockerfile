FROM openjdk:11.0.2-jdk-stretch
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} WordSliderApplication
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/WordSliderApplication"]