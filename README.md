## Word slides exercise

### Technologies Used / Developer Environment 
* Java 11
* Spring Boot <https://spring.io/projects/spring-boot>
* Install Docker Compose <https://docs.docker.com/compose/install/>
* Gradle Build Tool <https://gradle.org/>

 
### run in Intellij
 * checkout the project
 * `$ ./gradlew docker
 * create a gradle task in Run/Debug Configuration
 * `$ ./gradlew clean bootRun`
  * check http://localhost:8080/swagger-ui.html for possible operations

 
### run tests
 * checkout the project
 * `$ ./gradlew clean test`
 

### run as a docker-compose with gradle
 * checkout the project
 * ./gradlew docker build -x test
 * docker-compose up
 * check http://localhost:60001/swagger-ui.html for possible operations

### run as a docker-compose without gradle
 * docker build --build-arg JAR_FILE=path_to_the_application_jar -t wordslider .
 * docker-compose up
 
 
### Assumptions
 * "Longer slide" means that the slide length is bigger that is consists of more characters.
 * If slide is consumed only not whitespaces characters are consumed. 
 This means that from user input: "Mary went Mary's gone" and for two entries in a store: "went Mary's" and  "Mary gone"
 only first one will be matched. 
 
### TODO
 * Implement method call in asynchronous way
 * Write simple test to check if method is called asynchronously
 * Configure Redis serializer in order to ignore some properties
 * Move gradle versions to gradle.properties